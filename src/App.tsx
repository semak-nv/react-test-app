import React, { FunctionComponent } from 'react';
import { Dashboard } from './components/Dashboard';
import './App.css';

const App: FunctionComponent = () => (
  <Dashboard/>
);

export default App;
