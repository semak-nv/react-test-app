import { useEffect, useState } from 'react';
import { dataService } from '../servises/data-service';

export const useMonitorData = () => {
  const [temperature, setTemperature] = useState('N/A');
  const [pressure, setPressure] = useState('N/A');
  const [humidity, setHumidity] = useState('N/A');

  useEffect(() => {
    const subscription = dataService.getData()
      .subscribe(({ temperature, airPressure, humidity }) => {
        temperature && setTemperature(temperature);
        airPressure && setPressure(airPressure);
        humidity && setHumidity(humidity);
      });

    return () => subscription.unsubscribe();
  }, []);

  return {
    temperature,
    pressure,
    humidity,
  }
}
