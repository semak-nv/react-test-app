import { renderHook } from '@testing-library/react-hooks'
import { useMonitorData } from './useMonitorData';

describe('useMonitorData', () => {
  it('should use initial values ', () => {
    const { result } = renderHook(() => useMonitorData());

    expect(result.current.temperature).toBe('N/A');
    expect(result.current.pressure).toBe('N/A');
    expect(result.current.humidity).toBe('N/A');
  });
});
