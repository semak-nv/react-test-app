import { fromEvent, interval, merge, Observable, of } from 'rxjs';
import { catchError, concatMap, debounceTime, map, timeout } from 'rxjs/operators';
import { airPressure, humidity, temperature } from '../api/mock-data';

enum Parameters {
  Temperature = 'temperature',
  AirPressure = 'airPressure',
  Humidity = 'humidity'
}
interface Data {
  temperature?: string;
  airPressure?: string;
  humidity?: string;
}

class DataService {
  private debounceTime = 100;
  private timeoutTime = 1000;

  public getData(): Observable<Data> {
    return merge(
      this.fromEvent(temperature, Parameters.Temperature),
      this.fromEvent(airPressure, Parameters.AirPressure),
      this.fromEvent(humidity, Parameters.Humidity),
    ).pipe(debounceTime(this.debounceTime));
  }

  private fromEvent(target: NodeJS.EventEmitter, parameter: Parameters): Observable<Data> {
    return interval(this.timeoutTime)
      .pipe(
        concatMap(duration =>
          fromEvent(target, 'data').pipe(
            timeout(this.timeoutTime),
            catchError(error => of('N/A'))
          )
        ),
        map(value => ({ [parameter]: value })),
      );
  }
}

export const dataService = new DataService();
