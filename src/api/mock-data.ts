import events from 'events';

export const temperature = new events.EventEmitter();
export const airPressure = new events.EventEmitter();
export const humidity = new events.EventEmitter();

function emitTemperatureData() {
  const interval = Math.floor((Math.random() * 1900) + 100);
  const data = Math.floor((Math.random() * 50) + 200) / 10;
  temperature.emit('data', data.toFixed(1));
  setTimeout(() => emitTemperatureData(), interval);
}

function emitAirPressureData() {
  const interval = Math.floor((Math.random() * 1900) + 100);
  const data = Math.floor((Math.random() * 50) + 980);
  airPressure.emit('data', data.toString());
  setTimeout(() => emitAirPressureData(), interval);
}

function emitHumidityData() {
  const interval = Math.floor((Math.random() * 1900) + 100);
  const data = Math.floor((Math.random() * 20) + 40);
  humidity.emit('data', data.toString());
  setTimeout(() => emitHumidityData(), interval);
}

emitTemperatureData();
emitAirPressureData();
emitHumidityData();
