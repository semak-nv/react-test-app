import React from 'react';
import { render, screen } from '@testing-library/react';
import { Dashboard } from './Dashboard';

test('renders dashboard title', () => {
  render(<Dashboard />);
  const title = screen.getByText(/Medical Operating Room Monitor/i);
  expect(title).toBeInTheDocument();
});
