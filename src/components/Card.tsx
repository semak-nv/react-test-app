import React, { FunctionComponent } from 'react';

interface Props {
  title: string;
  value: string;
  dimension: string;
}

export const Card: FunctionComponent<Props> = ({ title, value, dimension }) => (
  <div className="card">
    <h2>{title}</h2>
    <div className="data">
      <span className="value">{value}</span>
      {' '}
      <span className="dimension">{dimension}</span>
    </div>
  </div>
)
