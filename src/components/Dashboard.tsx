import React, { FunctionComponent } from 'react';
import { Card } from './Card';
import { useMonitorData } from '../hooks/useMonitorData';

export const Dashboard: FunctionComponent = () => {
  const { temperature, pressure, humidity } = useMonitorData();

  return (
    <main>
      <h1>Medical Operating Room Monitor</h1>

      <div className="container">
        <Card title="Temperature" value={temperature} dimension="℃"/>
        <Card title="Air pressure" value={pressure} dimension="mbar"/>
        <Card title="Humidity" value={humidity} dimension="%"/>
      </div>
    </main>
  )
}
