import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Card } from './Card';

let container: Element;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
});

describe('Card', () => {
  it('renders title, value and dimension', () => {
    act(() => {
      render(<Card title="Temperature" value="24.2" dimension="℃"/>, container);
    });
    expect(container.querySelector('h2')?.textContent).toBe('Temperature');
    expect(container.querySelector('.value')?.textContent).toBe('24.2');
    expect(container.querySelector('.dimension')?.textContent).toBe('℃');
  });
});
